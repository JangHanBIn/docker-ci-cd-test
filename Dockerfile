FROM       golang:1.18
MAINTAINER espoirona09@gmail.com
RUN        apt-get -y update

COPY . /usr/src/app

WORKDIR /usr/src/app
RUN     make

CMD    ./app