package main

import "github.com/gin-gonic/gin"

func Index(c *gin.Context) {
	println("Hello")
	c.String(200, "Hello World!")
}

func main() {

	app := gin.New()
	v1 := app.Group("/v1")
	{
		v1.GET("/", Index)
	}

	app.Run(":8080")
}
